# README: Pixel Shift Code #


### What is this repository for? ###
Estimating pixel/distance shifts of a vessel during a UAS calibration survey of a vessel.

### How do I get set up? ###
The designated pixel shift code requires attitude change angles and not attitude angles. 
The motional file converter file can convert the recorded attitude angles into attitude 
change angles from the start of the UAS survey. Time intervals in this code are defined
to 1s or 15s. Once you have attitude change angles, you can put it through the pixel
shift code.

### Who do I talk to? ###
Casey O'Heran
